#    (c) 2009 Braydon Fuller. Some rights reserved.

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import gdbm
import simplejson as json
from math import radians, sin, cos, atan2, sqrt
from datetime import datetime


def get_node_and_ways(source):
    """
    Parses out the nodes, and ways from an OSM file, and stores the nodes
    into a dbm as key/value where the key is the node id, and the value is
    a json list of the latitude, and longitude. For the ways it stores it as
    key/value where key is the way id, and the value is a list of the node ids
    in the way path.
    """

    weights = {
      'motorway': 0,
      'motorway_link': 0,    
      'trunk': 1,
      'primary': 1,
      'primary_link': 1,
      'traffic_signals': 1,
      'turning_circle': 3,
      'gate': 1,
      'bus_stop': 1,
      'crossing': 1,
      'secondary': 2,
      'tertiary': 2,
      'unclassified': 2,
      'minor': 2,
      'cycleway': 3,
      'residential': 3,
      'track': 2,
      'service': 2,
      'bridleway': 1,
      'footway': 1,
      'steps': 1,
      'rail': 0,
      'light_rail': 0,
      'subway': 0
    }

    class NodeAndWayHandler(ContentHandler):
        def __init__(self):
            self.reset()

        def startElement(self, name, attrs):
            if name == 'node':
                nodes[attrs.get('id')] = json.dumps([attrs.get('lat'), attrs.get('lon')])
            if name == 'way':
                self.current_way = attrs.get('id')
            if name == 'nd':
                self.way_info[0].append(attrs.get('ref'))
            if name == 'tag':
                if attrs.get('k') == 'highway':
                    self.way_info[1] = weights[attrs.get('v')]
                if attrs.get('k') == 'oneway':
                    if attrs.get('v') == 'yes':
                        self.way_info[2] = 1

        def endElement(self, name):
            if name == 'way':
                if float(self.way_info[1]) > float(0):
                    ways[self.current_way] = json.dumps(self.way_info)
                self.reset()
            if name == 'node':
                self.reset()

        def reset(self):
            self.current_way = None
            self.way_info = [[], 0, 0]
    
    nodes = gdbm.open('nodes.dbm', 'c')
    ways = gdbm.open('ways.dbm', 'c')
    handler = NodeAndWayHandler()
    parser = make_parser()
    parser.setContentHandler(handler)
    datasource = open(source,'r')
    parser.parse(datasource)
    datasource.close()
    nodes.close()
    ways.close()    

#get_node_and_ways('downtownla.osm')    

def get_vertices():
    """
    Goes through every node and checks to see if that node is in two or more
    way paths, if it is then it is considered an intersection point, and added
    as a supernode. Vertices are stored as a list of their node ids.

    """
    vertices = []
    nodes = gdbm.open('nodes.dbm', 'r')
    ways = gdbm.open('ways.dbm', 'r')

    _ways = {}
    way = ways.firstkey()
    while way != None:
        _ways[way] = json.loads(ways[way])[0]
        way = ways.nextkey(way)    

    node = nodes.firstkey()
    while node != None:
        intersections = []

        for key, value in _ways.items():
            if node in value:
                intersections.append(key)

        if len(intersections) > 1:
            vertices.append(node)

        node = nodes.nextkey(node)
            
    nodes.close()
    ways.close()
    f = open('vertices','w')
    f.write(json.dumps(vertices))
    f.close()

def get_edges():
    """
    for each way, go through each node, check to see if it's a supernode,
    if not goto the next one and calculate the distance, and so on until
    hitting another supernode or the end of a way.
    """

    nodes = gdbm.open('nodes.dbm', 'r')    
    vertices = json.loads(open('vertices','r').read())
    ways = gdbm.open('ways.dbm', 'r')
    edges = {}


    _nodes = {}
    node = nodes.firstkey()
    while node != None:
        _nodes[node] = [float(x) for x in json.loads(nodes[node])]
        node = nodes.nextkey(node)

    def get_distance(x, y):
        """
        Calculates the distance in km between two nodes,
        using the haversine formula.
        """
        lat1, lon1 = _nodes[x]
        lat2, lon2 = _nodes[y]
        R = 6371.009
        dLat = radians(lat2-lat1)
        dLon = radians(lon2-lon1)
        a = sin(dLat/2.0) * sin(dLat/2.0) + \
            cos(radians(lat1)) * cos(radians(lat2)) * \
            sin(dLon/2.0) * sin(dLon/2.0)
        c = 2 * atan2(sqrt(a), sqrt(1.0-a))
        return(R * c)

    _ways = {}
    way = ways.firstkey()
    while way != None:
        _ways[way] = json.loads(ways[way])
        way = ways.nextkey(way)

    for sn in vertices:
        edges[sn] = {}

    def extract_edges(w):
        count = False
        distance = 0
        last_nd = None
        start_nd = None
        for nd in w[1][0]:
            if nd in vertices and count == True:
                edges[start_nd][nd] = distance
                if w[1][2] == 0:
                    edges[nd][start_nd] = distance
                start_nd = nd
                distance = 0            
            elif nd in vertices and count == False:
                count = True
                start_nd = nd
                distance = 0
            if count and last_nd:
                distance += get_distance(last_nd, nd)
            last_nd = nd

    map(extract_edges, _ways.items())
    f = open('edges','w')
    f.write(json.dumps(edges))
    f.close()

#get_edges()

def find_route(start_node, end_node):
    """
    Finds the shortest path between the start_node and the end_node.
    """
    
    vertices = json.loads(open('vertices','r').read())    
    edges = json.loads(open('edges','r').read())

    start_time = datetime.now()
    
    infinity = 1e10000
    nodes = {}

    for k in vertices:
        nodes[str(k)] = [infinity, False, None]
    nodes[start_node][0] = 0

    Q = [start_node]

    for node_id in Q:
        if node_id == end_node:
            break
        neighbors = edges[node_id]
        for key, value in neighbors.items():
            distance = nodes[node_id][0] + value
            if distance < nodes[str(key)][0]:
                nodes[str(key)][0] = distance
                nodes[str(key)][2] = node_id
        nodes[node_id][1] = True
        for key in neighbors.keys():
            if not nodes[str(key)][1]:
                Q.append(key)

    route = [end_node]
    R = [end_node]
    for n in R:
        previous = nodes[n][2]
        if previous:
            route.append(previous)
            R.append(previous)

    route.reverse()

    total_time = round(float((datetime.now() - start_time).microseconds)/1000000.0, 2)

    print 'start: %s, end: %s, distance: %s, osm_route_ids: %s, calculation_time: %s seconds' % (start_node, end_node, nodes[end_node][0], route, total_time)

#find_route('21098551', '122656750')
